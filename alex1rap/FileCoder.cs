using System;
using System.Text;
using System.IO;

namespace Coder.alex1rap
{
    public class FileCoder
    {
        protected const int MIN_PART_SIZE = 128;
        protected const int MAX_PART_SIZE = 1024 * 512;
        FileInfo file;

        public FileCoder(string fileName)
        {
            this.file = new FileInfo(fileName);
        }

        public FileCoder(FileInfo file)
        {
            this.file = file;
        }

        protected static int GetPartSize(long fileSize)
        {
            int partSize = MIN_PART_SIZE;

            if (fileSize >= MAX_PART_SIZE * 10)
            {
                partSize = MAX_PART_SIZE;
            }
            else if (fileSize >= MAX_PART_SIZE)
            {
                partSize = 1024 * 64;
            }
            else if (fileSize >= 1024 * 64)
            {
                partSize = 512;
            }

            if (partSize < MIN_PART_SIZE)
            {
                partSize = MIN_PART_SIZE;
            }

            return partSize;
        }

        public string Encode(byte[] passHash, byte passLength)
        {
            FileInfo file = this.file;
            string fileName = file.Name;
            if (file.Exists)
            {
                try
                {
                    long fileSize = file.Length;
                    int partSize = GetPartSize(fileSize);
                    long parts = (long) decimal.Ceiling(fileSize / partSize);
                    if (fileSize / partSize > parts)
                    {
                        parts++;
                    }

                    TextCoder coder = new TextCoder();
                    coder.SetSize(fileSize);

                    DirectoryInfo encodedDir = new DirectoryInfo("./encrypted");
                    if (!encodedDir.Exists)
                    {
                        encodedDir.Create();
                    }

                    string resultName = coder.Encode(Encoding.Default.GetBytes(fileName), passHash, passLength, true);
                    resultName = resultName.Replace(@"/", "_u_").Replace(@"\", "_w_");
                    resultName = encodedDir.FullName + "/" + resultName;
                    FileInfo result = new FileInfo(resultName);
                    try
                    {
                        BinaryReader reader = new BinaryReader(file.OpenRead());
                        BinaryWriter writer = new BinaryWriter(result.OpenWrite());

                        for (long partIndex = 0; partIndex <= parts; partIndex++)
                        {
                            byte[] part = reader.ReadBytes(partSize);
                            if (part.Length > 0)
                            {
                                byte[] encoded = coder.Encode(part, passHash, passLength);
                                writer.Write(encoded, 0, encoded.Length);
                            }
                        }
                        reader.Close();
                        writer.Close();
                    }
                    catch (System.Exception e)
                    {
                        return "При записи в файл '" + result.FullName + "' произошла ошибка: " + e.Message;
                    }

                    File.Delete(file.FullName);
                    return "В файл '" + result.FullName + "' записано " + result.Length + " байт.\nИсходный файл '" +
                           file.FullName + "' удален.";
                }
                catch (UnauthorizedAccessException e)
                {
                    return "Невозможно получить доступ к файлу '" + file.FullName + "'. Файл пропущен.";
                }
            }
            return "Файл " + file.FullName + " не существует.";
        }

        public string Decode(byte[] passHash, byte passLength)
        {
            FileInfo file = this.file;
            string fileName = file.Name;
            if (file.Exists)
            {
                try
                {
                    fileName = fileName.Replace("_u_", @"/").Replace("_w_", @"\");
                    long fileSize = file.Length;
                    int partSize = GetPartSize(fileSize);
                    long parts = (long) decimal.Ceiling(fileSize / partSize);
                    if (fileSize / partSize > parts)
                    {
                        parts++;
                    }

                    TextCoder coder = new TextCoder();
                    coder.SetSize(fileSize);
                    string resultName = Encoding.Default.GetString(coder.Decode(fileName, passHash, passLength));
                    if (resultName == null)
                    {
                        return "Невозможно расшифровать название файла '" + file.FullName + "'. Будет пропущен.";
                    }

                    DirectoryInfo decodedDir = new DirectoryInfo("./decrypted");
                    if (!decodedDir.Exists)
                    {
                        decodedDir.Create();
                    }
                    resultName = resultName.Replace(@"\", "_w_").Replace(@"/", "_u_").Replace(@":", "_d_");
                    resultName = decodedDir.FullName + "/" + resultName;
                    FileInfo result = new FileInfo(resultName);
                    try
                    {
                        BinaryReader reader = new BinaryReader(file.OpenRead());
                        BinaryWriter writer = new BinaryWriter(result.OpenWrite());

                        for (long partIndex = 0; partIndex <= parts; partIndex++)
                        {
                            byte[] part = reader.ReadBytes(partSize);
                            if (part.Length > 0)
                            {
                                byte[] decoded = coder.Encode(part, passHash, passLength);
                                writer.Write(decoded, 0, decoded.Length);
                            }
                        }
                        reader.Close();
                        writer.Close();
                    }
                    catch (System.Exception e)
                    {
                        return "При записи в файл '" + result.FullName + "' произошла ошибка: " + e.Message;
                    }

                    File.Delete(file.FullName);
                    return "В файл '" + result.FullName + "' записано " + result.Length + " байт.\nИсходный файл '" +
                           file.FullName + "' удален.";
                }
                catch (UnauthorizedAccessException e)
                {
                    return "Невозможно получить доступ к файлу '" + file.FullName + "'. Файл пропущен.";
                }
                catch (ArgumentNullException e)
                {
                    return "Невозможно обработать файл '" + file.FullName + "'. Будет пропущен.";
                }
            }
            else
            {
                return "Файл " + file.FullName + " не существует.";
            }
        }
    }
}
