using System;
using System.Text;

namespace Coder.alex1rap
{
    public class TextCoder
    {
        protected long size = 0;
        protected long count = 0;

        public void SetSize(long size)
        {
            this.size = size;
        }

        protected byte[] DoCode(byte[] source, byte[] passHash, byte passLength)
        {
            if (source.Length == 0)
            {
                return null;
            }
            for (int i = 0; i < source.Length; i++)
            {
                byte code = (byte) (source[i] ^ passHash[i % passHash.Length] ^ passLength);
                source[i] = code;
                this.count++;
            }
            //Console.Write("\rОбработано {0}/{1} байт...", this.count, this.size);
            return source;
        }

        public string Encode(byte[] source, byte[] passHash, byte passLength, bool withBase64)
        {
            if (source.Length == 0)
            {
                return null;
            }
            byte[] result = DoCode(source, passHash, passLength);
            return withBase64 ? System.Convert.ToBase64String(result, 0, result.Length, Base64FormattingOptions.None) : Encoding.Default.GetString(result);
        }

        public byte[] Encode(byte[] source, byte[] passHash, byte passLength)
        {
            if (source.Length == 0)
            {
                return null;
            }
            byte[] result = DoCode(source, passHash, passLength);
            return result;
        }

        public byte[] Decode(string source, byte[] passHash, byte passLength)
        {
            try
            {
                if (source == null || source.Length == 0)
                {
                    return null;
                }

                return DoCode(System.Convert.FromBase64String(source), passHash, passLength);
            }
            catch (ArgumentNullException e)
            {
                return null;
            }
            catch (FormatException e)
            {
                return null;
            }
        }

        public byte[] Decode(byte[] source, byte[] passHash, byte passLength)
        {
            return DoCode(source, passHash, passLength);
        }
    }
}
