# CS-FileCrypter

**Console file crypter by password**

Supported commands:
*  (**e**)**xit**, (**q**)**uit**, (**s**)**top** - to close the program
*  **cp**, **chpwd**, **change-pass**, **change-password** - to change crypt password
*  (**enc**)**ode** - encode one file or all files in directory
*  (**dec**)**ode** - decode one file or all files in directory
