using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace Coder
{
    class Program
    {
        protected const int MAX_THREADS = 15;
        protected bool runned;
        protected string pass;
        protected byte[] passHash;
        protected byte passLength = 0;

        protected void WriteStars(int count)
        {
            Console.Write("\r" + new string('*', count) + new String('\0', 32 - count));
            Console.SetCursorPosition(count, Console.CursorTop);
        }

        protected void ChangePass()
        {
            Console.WriteLine("Введите пароль шифрования:");
            try
            {
                bool entered = false;
                string pass = "";
                string stars = "";
                while (!entered)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);
                    int last = pass.Length > 0 ? pass.Length - 1 : 0;
                    switch (cki.Key)
                    {
                        case ConsoleKey.Enter:
                            entered = true;
                            continue;
                        case ConsoleKey.Backspace:
                            if (pass.Length > 0)
                            {
                                stars = stars.Remove(last);
                                pass = pass.Remove(last);
                                this.WriteStars(pass.Length);
                            }
                            break;
                        default:
                            if (cki.KeyChar > 0)
                            {
                                stars += "*";
                                pass += cki.KeyChar.ToString();
                            }
                            this.WriteStars(pass.Length);
                            break;
                    }
                }
                Console.WriteLine();
                this.pass = pass;
            } catch (System.IO.IOException e)
            {
                Console.WriteLine("Исключение: " + e.Message);
            } catch (System.ArgumentOutOfRangeException e)
            {
                Console.WriteLine("Исключение: " + e.Message);
            }

            while (string.IsNullOrEmpty(this.pass))
            {
                this.ChangePass();
            }
            this.passLength = (byte) this.pass.Length;
            this.passHash = Encoding.Default.GetBytes(this.CreateHash(this.pass));
            Console.WriteLine("Введите команду:");
        }

        protected string CreateHash(string password)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] input = Encoding.Default.GetBytes(password);
            byte[] output = md5.ComputeHash(input);
            StringBuilder sb = new StringBuilder();
            foreach (byte b in output)
            {
                sb.Append(b.ToString("x"));
            }
            return sb.ToString();
        }

        protected void Run()
        {
            this.runned = true;
            while (runned)
            {
                if (string.IsNullOrEmpty(this.pass))
                {
                    this.ChangePass();
                }

                string cmd = Console.ReadLine();
                if (!string.IsNullOrEmpty(cmd))
                {
                    switch(cmd)
                    {
                        case "e":
                        case "q":
                        case "s":
                        case "exit":
                        case "stop":
                        case "quit":
                            this.runned = false;
                            continue;
                        case "cp":
                        case "chpwd":
                        case "change-pass":
                        case "change-password":
                            this.ChangePass();
                            break;
                        case "enc":
                        case "encode":
                            this.Encode();
                            break;
                        case "dec":
                        case "decode":
                            this.Decode();
                            break;
                        default:
                            Console.Error.WriteLine("Неизвестная команда '" + cmd + "'. Используйте:");
                            Console.WriteLine("- '(e)xit|(s)top|(q)uit' чтобы закрыть приложение;");
                            Console.WriteLine("- 'cp|chpwd|change-pass|change-password' чтоб изменить пароль шифрования;");
                            Console.WriteLine("- '(enc)ode|(dec)ode' чтоб зашифровать|расшифровать следующую строку;");
                            break;
                    }
                    Console.WriteLine();
                }
            }
        }

        protected void Encode()
        {
            try
            {
                Console.WriteLine("\nВведите путь к файлу или папке, которые нужно зашифровать:");
                string line = Console.ReadLine();
                if (Directory.Exists(line))
                {
                    DirectoryInfo directory = new DirectoryInfo(line);
                    FileInfo[] files = directory.GetFiles();
                    if (files.Length > 0)
                    {
                        System.Threading.Tasks.Parallel.ForEach(
                            files,
                            new System.Threading.Tasks.ParallelOptions {MaxDegreeOfParallelism = MAX_THREADS},
                            file =>
                            {
                                try
                                {
                                    alex1rap.FileCoder fileCoder = new alex1rap.FileCoder(file.FullName);
                                    string encoded = fileCoder.Encode(this.passHash, this.passLength);
                                    Console.WriteLine(encoded);
                                    Console.WriteLine();
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("\nПроизошла неизвестная ошибка в одном из процессов: " + e.Message);
                                }
                            });
                    }
                    else
                    {
                        Console.WriteLine("\nПапка '" + directory.FullName + "' не содержит файлов.");
                    }
                }
                else if (File.Exists(line))
                {
                    alex1rap.FileCoder fileCoder = new alex1rap.FileCoder(line);
                    string encoded = fileCoder.Encode(this.passHash, this.passLength);
                    Console.WriteLine(encoded);
                }
                else
                {
                    Console.WriteLine("\nФайла или папки с названием '" + line + "' не найдено.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nПроизошла неизвестная ошибка: " + e.Message);
            }
        }

        protected void Decode()
        {
            try
            {
                Console.WriteLine("\nВведите путь к файлу или папке, которые нужно расшифровать:");
                string line = Console.ReadLine();
                if (Directory.Exists(line))
                {
                    DirectoryInfo directory = new DirectoryInfo(line);
                    FileInfo[] files = directory.GetFiles();
                    if (files.Length > 0)
                    {
                        System.Threading.Tasks.Parallel.ForEach(
                            directory.GetFiles(),
                            new System.Threading.Tasks.ParallelOptions {MaxDegreeOfParallelism = MAX_THREADS},
                            file =>
                            {
                                try
                                {
                                    alex1rap.FileCoder fileCoder = new alex1rap.FileCoder(file.FullName);
                                    string decoded = fileCoder.Decode(this.passHash, this.passLength);
                                    Console.WriteLine(decoded);
                                    Console.WriteLine();
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("\nПроизошла неизвестная ошибка в одном из процессов: " + e.Message);
                                }
                            });
                    }
                    else
                    {
                        Console.WriteLine("\nПапка '" + directory.FullName + "' не содержит файлов.");
                    }
                }
                else if (File.Exists(line))
                {
                    alex1rap.FileCoder fileCoder = new alex1rap.FileCoder(line);
                    string decoded = fileCoder.Decode(this.passHash, this.passLength);
                    Console.WriteLine(decoded);
                }
                else
                {
                    Console.WriteLine("\nФайла или папки с названием '" + line + "' не найдено.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nПроизошла неизвестная ошибка: " + e.Message);
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("File Crypter by alex1rap: https://vk.me/alex1rap \n");
            Program app = new Program();
            app.Run();
        }
    }
}
